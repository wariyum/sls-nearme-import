const productCsvValidate = require("./schema-validateCSV/product-schema")
const categoryCsvValidate = require("./schema-validateCSV/category-schema")
const brandCsvValidate = require("./schema-validateCSV/brand-schema")
const imageCsvValidate = require("./schema-validateCSV/image-schema")
const { S3Client, GetObjectCommand } = require("@aws-sdk/client-s3");
let nearmeTransLog = require("/opt/transLogEntry.js")
// Create an Amazon S3 service client object.
const s3Client = new S3Client({ region: process.env.AWS_REGION });
const fs = require("fs");
const NEARME_ERROR_CODE = "WF_EAN-CSV_VALIDATION"
const NEARME_TRANS_TYPE = "Import-EAN"
/**
 * to get the csv validated file
 * @param {*} event
 */
module.exports.validateCsv = async (event, context) => {
    let s3Paths = event
    let s3PathToValidate = s3Paths.sourceS3Path
    // let s3PathToValidate = "nearme-ean-import/4-1-NER-1/_processed/product/1productList4-14-2023T14-38.csv"
    //nearme-ean-import/4-1-NER-1/_processed/product/1productList4-14-2023T14-38.csv"
    let s3PathSplitValues = s3PathToValidate.split("/");
    let [s3Bucketname, pgmId, , csvType, importedFileName] = s3PathSplitValues
    //removing the bucket path from the list values
    s3PathSplitValues.shift()
    let s3Path = s3PathSplitValues.join("/")
    try {
        let s3ObjectPath = await readObjectFromS3(s3Bucketname, s3Path, importedFileName)
        let validatedData = {};
        console.log("Validating CSV Type: " + csvType);
        switch (csvType) {
            case "product": {
                validatedData = await productCsvValidate.productValidateCsv(s3ObjectPath)
                break;
            }
            case "category": {
                validatedData = await categoryCsvValidate.categoryValidateCsv(s3ObjectPath)
                break;
            }
            case "brand": {
                validatedData = await brandCsvValidate.brandValidateCsv(s3ObjectPath)
                break;
            }
            case "images": {
                validatedData = await imageCsvValidate.imageValidateCsv(s3ObjectPath)
                break;
            }
        }
        //Deleting the tmp file 
        deleteTmpFile(s3ObjectPath)
        // in case if invalid data empty return product json data or invaliddata is returned
        if (validatedData.inValidData == "") {
            return s3Paths;
        } else {
            //Error entering in translog DB
            await nearmeTransLog.captureErrorToTransErrorDb("CSV-Validate", importedFileName, pgmId, JSON.stringify(validatedData.inValidData), NEARME_ERROR_CODE, NEARME_TRANS_TYPE)
            //removing the source Path from the s3 Path for step function to not to process to the next process, the CSV file is not valid
            s3Paths.sourceS3Path = "";
            return s3Paths;
        }

    }
    catch (err) {
        nearmeTransLog.captureErrorToTransErrorDb("read", importedFileName, pgmId, err.message, NEARME_ERROR_CODE, NEARME_TRANS_TYPE)
    }

}

/**
 * Read the file that is downloaded from s3
 * @param {*} s3BucketName 
 * @param {*} s3UploadedFilePath 
 * @param {*} importedFileName 
 * @returns 
 */
function readObjectFromS3(
    s3BucketName,
    s3UploadedFilePath,
    importedFileName
) {
    const command = new GetObjectCommand({
        Bucket: s3BucketName,
        Key: s3UploadedFilePath,
    });
    //write the downloaded file
    return new Promise(async (resolve, reject) => {
        const item = await s3Client.send(command);
        // Read a file from s3
        item.Body.pipe(fs.createWriteStream("/tmp/" + importedFileName))
            .on("error", (err) => {
                reject(err)
            })
            .on("close", () => {
                resolve("/tmp/" + importedFileName)
            });
    });
};

/**
 * Deleting the temp file
 * @param {*} filePath 
 */
async function deleteTmpFile(filePath) {
    try {
        fs.unlinkSync(filePath);
    } catch (err) {
        console.error(err);
    }
}