const csvValidator = require("csv-file-validator");
const fs = require("fs");


/**
 * Read the file that is downloaded from s3 and validate
 * @param {*} filePath 
 * @returns 
 */
module.exports.productValidateCsv = async (filePath) => {
    let fileContents = fs.readFileSync(filePath, "utf8");
    const config = {
        headers: [
            {
                name: "id",
                inputName: "id",
                required: false,
            },
            {
                name: "name",
                inputName: "name",
                required: true,
            },
            {
                name: "brand",
                inputName: "brand",
                required: false,
            },
            {
                name: "categories",
                inputName: "categories",
                required: false,
            },
            {
                name: "description",
                inputName: "description",
                required: false,
            },
            {
                name: "productType",
                inputName: "productType",
                required: true,
                validate: function (productType) {
                    return isValidProductType(productType);
                },
            },
            {
                name: "SP_skuId",
                inputName: "SP_skuId",
                required: false,
            },
            {
                name: "SP_salePrice",
                inputName: "SP_salePrice",
                required: false,
                validate: function (SP_salePrice) {
                    return isValidSalePrice(SP_salePrice);
                },
            },
            {
                name: "SP_mrp",
                inputName: "SP_mrp",
                required: false,
                validate: function (SP_mrp) {
                    return isValidSpMrp(SP_mrp);
                },
            },
            {
                name: "SP_images",
                inputName: "SP_images",
                required: false,
            },
            {
                name: "productItemType",
                inputName: "productItemType",
                required: true,
                validate: function (productItemType) {
                    return isValidName(productItemType);
                }
            },

            {
                name: "productItemType2",
                inputName: "productItemType2",
                required: false,
            },
            {
                name: "otherProductProps",
                inputName: "otherProductProps",
                required: false,
            },
            {
                name: "isHidden",
                inputName: "isHidden",
                required: false,
            },
            {
                name: "isFeatured",
                inputName: "isFeatured",
                required: false,
            },
            {
                name: "label",
                inputName: "label",
                required: true,
                validate: function (label) {
                    return isValidLabel(label);
                },
            },
            {
                name: "searchSuggestion",
                inputName: "searchSuggestion",
                required: false,
            },
            {
                name: "SP_discount",
                inputName: "SP_discount",
                required: false,
            },
            {
                name: "SP_isOutofstock",
                inputName: "SP_isOutofstock",
                required: false,
            },
            {
                name: "SP_skuRule",
                inputName: "SP_skuRule",
                required: false,
            },
            {
                name: "stockManaged",
                inputName: "stockManaged",
                required: false,
            },
            {
                name: "isStockManaged",
                inputName: "iisStockManaged",
                required: false,
            },
            {
                name: "supplierId",
                inputName: "supplierId",
                required: false,
            },
            {
                name: "warehouseId",
                inputName: "warehouseId",
                required: false,
            },
            {
                name: "dataSource",
                inputName: "dataSource",
                required: false,
            },
            {
                name: "sortPriority",
                inputName: "sortPriority",
                required: false,
            },
            {
                name: "VP_name",
                inputName: "VP_name",
                required: false,
            },
            {
                name: "VP_discount",
                inputName: "VP_discount",
                required: false,
            },
            {
                name: "VP_isOutofstock",
                inputName: "VP_isOutofstock",
                required: false,
            },
            {
                name: "VP_sku",
                inputName: "VP_sku",
                required: false,
            },
            {
                name: "VP_salePrice",
                inputName: "VP_salePrice",
                required: false,
            },
            {
                name: "VP_mrp",
                inputName: "VP_mrp",
                required: false,
            },
            {
                name: "VP_skuRule",
                inputName: "VP_skuRule",
                required: false,
            },
            {
                name: "VP_images",
                inputName: "VP_images",
                required: false,
            }
        ],
    };
    // validate the csv file
    return csvValidator(fileContents, config);
}

/**
 * validate header productType can be either variant product or simple product
 * @param {*} productType
 * @returns
 */
function isValidProductType(productType) {
    var productFormat = /VARIANT_PRODUCT|SIMPLE_PRODUCT/;
    if (productType !== "" && productType.match(productFormat)) {
        return true;
    }
    return false;
}

/**
 * validate SP_salePrice ie digits,decimals points,or can be empty
 * @param {*} SP_salePrice
 * @returns
 */
function isValidSalePrice(SP_salePrice) {
    var spFormat = /^[0-9]\d*(\.\d+)?$/;
    if (SP_salePrice == "" || SP_salePrice.match(spFormat)) {
        return true;
    }
    return false;
}

/**
 * validate SP_mrp  ie digits,decimals points,or can be empty
 * @param {*} SP_mrp
 * @returns
 */
function isValidSpMrp(SP_mrp) {
    var spFormat = /^[0-9]\d*(\.\d+)?$/;
    if (SP_mrp == "" || SP_mrp.match(spFormat)) {
        return true;
    }
    return false;
}
/**
 * 
 * @param {*} name 
 * @returns 
 */

function isValidName(productItemType) {
    var productItem = /^[A-Za-z0-9\s]*$/;
    if (productItemType !== "" && productItemType.match(productItem)) {
        return true;
    }
    return false;
}

/**
 * validate label matches NONE,VEG or NON-VEG
 * @param {*} label
 * @returns
 */
function isValidLabel(label) {
    var labelFormat = /NONE|VEG|NON-VEG/g;
    if (label !== " " && label.match(labelFormat)) {
        return true;
    }
    return false;
}


