const csvValidator = require("csv-file-validator");
const fs = require("fs");
/**
 * Read the file that is downloaded from s3 and validate
 * @param {*} filePath 
 * @returns 
 */
module.exports.brandValidateCsv = async (filePath) => {
    let fileContents = fs.readFileSync(filePath, "utf8");
    const config = {
        headers: [
            {
                name: "id",
                inputName: "id",
                required: false,
            },
            {
                name: "name",
                inputName: "name",
                required: true,
            },
            {
                name: "images",
                inputName: "images",
                required: false,
            },
            {
                name: "priority",
                inputName: "priority",
                required: false,
            },

        ]
    }
    // validate the csv file
    return csvValidator(fileContents, config)
}
