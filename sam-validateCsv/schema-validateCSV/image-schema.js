const csvValidator = require("csv-file-validator");
const fs = require("fs");
/**
 * Read the file that is downloaded from s3 and validate
 * @param {*} filePath 
 * @returns 
 */
module.exports.imageValidateCsv= async (filePath) => {

    let fileContents = fs.readFileSync(filePath, "utf8");
    const config = {
        headers: [
            {
                name: "imageUrl",
                inputName: "imageUrl",
                required: true,
            },
            {
                name: "suggestedImageName",
                inputName: "suggestedImageName",
                required: false,
            },
        ]
    }
     // validate the csv file
    return csvValidator(fileContents, config)
 
}
