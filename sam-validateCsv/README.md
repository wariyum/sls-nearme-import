## Validate Import CSV's
========================================================
* Input : {sourceS3path}
  - sourceS3Path: nearme-ean-import/{pgmId}/_processed/{csvType}/{importedFIleName}
  - Example: sourceS3Path: nearme-ean-import/4-1-NER-1/_processed/product/1productList3-16-2023T23-45.csv

* Validate CSV based on the {csvType - product, brand, categories and image}
  - CSV type is read from sourceS3Path

* If the CSV validation is success return the sourceS3Path and destination path to import the CSV file
  else return empty array of source path 

