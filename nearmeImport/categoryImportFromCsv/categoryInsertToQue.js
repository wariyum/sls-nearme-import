const { SQSClient, SendMessageCommand } = require("@aws-sdk/client-sqs");
const sqsClient = new SQSClient({ region: process.env.AWS_REGION });
const ssm = require("/opt/config.js")
let nearmeTransLog = require("/opt/transLogEntry.js")
const NEARME_TRANS_TYPE = "Import-Categories"
const NEARME_ERROR_CODE = "SS_TL-CATEGORY-INSERT-QUEUE"
let count = 0;
/**
 * Category payload insert to the SQS queue
 * @param {*} pgmId 
 * @param {*} messageBody 
 * @returns 
 */
module.exports = async function categoryInsertToQueue(pgmId, messageBody, importGroupId) {
    //pgmId adding to the product payload
    messageBody.hashKey = pgmId;
    // Create an SQS service object
    const queueUrl = await ssm.getValueFromSSM("categoryCreateQueUrl")
    let deDuplicationId = ""
    //deDuplicationId set for TASK COMPLETE message
    if (messageBody.s3Task != undefined && messageBody.s3Task == "TASK-COMPLETED") {
        deDuplicationId = "taskComplete"
        messageBody.fileName = importGroupId
    } else {
        //for categoryPayload in messageBody set category name has the duplicationId
        deDuplicationId = messageBody.name.replace(/[^a-zA-Z0-9]/g, "")
    }
    let params = {
        MessageBody: JSON.stringify(messageBody),
        QueueUrl: queueUrl,
        MessageDeduplicationId: deDuplicationId,  // Required for FIFO queues
        MessageGroupId: importGroupId //imorted product csv filename
    };
    await sqsClient.send(new SendMessageCommand(params))
        .then(response => console.log(++count + " " + response.MessageId))
        .catch(err => {
            //nearme trans error log details entry in DB for category insert to queue
            nearmeTransLog.captureErrorToTransErrorDb(messageBody.name, importGroupId, pgmId, err.message, NEARME_ERROR_CODE, NEARME_TRANS_TYPE)

        });
}