const aws4 = require('aws4');
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const ssm = require("/opt/config.js")
let nearmeTransLog = require("/opt/transLogEntry.js")
let NEARME_ERROR_CODE = "SS_TL-CATEGORY-API-FAILURE"

/**
 * function to invoke category creation API using Node-fetch
 * @param {*} pgmId 
 * @param {*} categoryPayload 
 * @param {*} categoryCreationApi 
 * @param {*} apiMethod 
 * @param {*} messageGrpId 
 * @param {*} nearmeTxnType 
 * @returns 
 */
module.exports = async function invokeCategoryApi(pgmId, categoryPayload, categoryCreationApi, apiMethod, messageGrpId, nearmeTxnType) {
  const accessKey = await ssm.getValueFromSSM("accesskey");
  const secretKey = await ssm.getValueFromSSM("secretkey");
  // aws4 to authenticate api
  const url = new URL(categoryCreationApi);
  let pathQueryParams = url.pathname
  //query parameter setting on params
  if (url.searchParams) {
    pathQueryParams = pathQueryParams + "?" + url.searchParams.toString();
  }
  const params = {
    method: apiMethod,
    host: url.host,
    path: pathQueryParams,
    service: 'execute-api',
    body: JSON.stringify(categoryPayload),
    region: process.env.AWS_REGION,
    headers: {
      "content-type": "application/json",
      "war-pgm": `${pgmId}`
    }
  };
  //removing body in params for GET Request
  if (apiMethod == "GET") {
    delete params.body;
  }
  const signature = aws4.sign(params, { accessKeyId: accessKey, secretAccessKey: secretKey, });
  //invoke category API and create category 
  return fetch(url, signature).then(async response => {
    let responseBody = await response.json();
    if (responseBody.success != null) {
      //get category API response
      if (responseBody.success.content != undefined) {
        let categoryList = responseBody.success.content
        //no match found for category name return null
        if (categoryList.length == 0) {
          return null
        }
        //returning first category id found from API response
        return categoryList[0].id
      }
      //created brand response POST 
      else {
        console.log("Created Category " + responseBody.success.name)
        //returning created brand id
        return responseBody.success.id
      }
    }
    else {
      //category upsert API failure error entry in trasLog table
      nearmeTransLog.captureErrorToTransErrorDb(categoryPayload.name, messageGrpId, pgmId, responseBody.error.errorCode, NEARME_ERROR_CODE, nearmeTxnType)
      throw new Error(responseBody)
    }
  })
    .catch(err => {
      nearmeTransLog.captureErrorToTransErrorDb(categoryPayload.name, messageGrpId, pgmId, err.message, NEARME_ERROR_CODE, nearmeTxnType)
      throw err
    })
}
