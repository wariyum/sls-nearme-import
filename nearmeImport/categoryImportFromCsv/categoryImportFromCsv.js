const categoryInsertToQue = require('./categoryInsertToQue.js');
const invokeCategoryAPI = require('./categoryCreationApi.js')
const ssm = require("/opt/config.js")
const NEARME_TXN_TYPE = "Import-Categories";

/**
 * Function for importing categories to a pgmId 
 * @param {*} categoryCsvList 
 * @param {*} pgmId 
 * @param {*} importGroupId  -imported fileName
 */
module.exports = async function importCategoriesFromCsv(categoryCsvList, pgmId, importGroupId) {
    const s3ImageLocation = await ssm.getValueFromSSM("s3imagePrefixUrl")
    const imagePrefix = s3ImageLocation + `${pgmId}/image/categories/`
    let indexOfCsv = 0;
    let categoriesToQue = [];
    while (indexOfCsv < categoryCsvList.length) {
        let categoryCsvItem = categoryCsvList[indexOfCsv];
        indexOfCsv++;
        let categoryPayload = {}
        if (categoryCsvItem.id) {
            categoryPayload.id = categoryCsvItem.id;
        }
        //categoryName check if exist in db with '?categoryName'
        if (categoryCsvItem.name.charAt(0) == '?') {
            let categoryExist = await checkCategoryExist(pgmId, categoryCsvItem.name.slice(1), importGroupId);
            //if exist skip the creation(pushing the category payload to category insert queue) of category
            if (categoryExist) {
                continue;
            }
            //else create a new category with category name
            else {
                categoryPayload.name = categoryCsvItem.name.slice(1);
            }
        } else {
            categoryPayload.name = categoryCsvItem.name;
        }
        categoryPayload.description = categoryCsvItem.description
        categoryPayload.parentId = categoryCsvItem.parentId
        categoryPayload.nameHash = categoryPayload.name.toLowerCase();
        categoryPayload.priority = categoryCsvItem.priority
        categoryPayload.isHidden = categoryCsvItem.isHidden.toLowerCase() == "true";
        categoryPayload.hasProduct = categoryCsvItem.hasProduct.toLowerCase() == "true"
        if (categoryCsvItem.hasProduct == "TRUE") {
            categoryPayload.hasCategory = false;
        }
        else {
            categoryPayload.hasCategory = true;
        }
        categoryPayload.images = [];
        if (categoryCsvItem.images) {
            let imageItems = {}
            let imageFileName = categoryCsvItem.images;
            let correctedImageFileName = imageFileName.replace(";", "");
            imageItems.url = imagePrefix + correctedImageFileName;
            imageItems.imageLocation = "S3"
            categoryPayload.images.push(imageItems);
        }
        categoriesToQue.push(categoryInsertToQue(pgmId, categoryPayload, importGroupId))
    };
    let taskProcessedMsg = {
        s3Task: "TASK-COMPLETED"
    }
    //After sending the categoryPayloads to FIFO queue the TASK COMPLETE message is passed
    categoriesToQue.push(categoryInsertToQue(pgmId, taskProcessedMsg, importGroupId))
    //Payloads are pushed to queue
    await Promise.all(categoriesToQue)
}

/**
 * Check the brandExist in db
 * @param {*} pgmId 
 * @param {*} categoryName 
 * @returns 
 */
async function checkCategoryExist(pgmId, categoryName, importGroupId) {
    let getCategoryApi = await ssm.getValueFromSSM("categoryGetApi") + encodeURIComponent(categoryName);
    //get categoryId if the category already exist 
    let id = await invokeCategoryAPI(pgmId, "", getCategoryApi, "GET", importGroupId, NEARME_TXN_TYPE)
    if (id != null) {
        return true;
    }
    return false;
}