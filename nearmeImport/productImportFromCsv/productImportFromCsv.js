const fs = require('fs')
const { Validator } = require('jsonschema');
const insertToQue = require('./productInsertToQue.js')
const invokeBrandAPI = require('../brandsImportFromCsv/brandCreationApi.js')
const invokeCategoryAPI = require('../categoryImportFromCsv/categoryCreationApi.js')
var validator = new Validator();
const VARIANT_PRODUCT = 'VARIANT_PRODUCT';
const config = require('/opt/config.js');
let productsWithImportIssues = [];
let nearmeTransLog = require("/opt/transLogEntry.js")
const NEARME_TRANS_TYPE = "Import-Products"
const NEARME_APP_IDENTIFIER = "SS_TL"

/**
 * Product import to Event Source Table after schema validation and also show products failed for product import
 * @param {*} productCsvList 
 * @param {*} pgmId 
 * @param {*} importGroupId Unique ID of import batch
 */
module.exports = async function importProductsFromCsv(productCsvList, pgmId, importGroupId) {
  //Lambda only allows you to write to and read from the "/tmp" directory
  let indexOfCsv = 0;
  let promiseTask = [];
  const s3ImageLocation = await config.getValueFromSSM("s3imagePrefixUrl")
  //TODO: Read schema from DynamoDB config table
  let productSchemaOfSimple = await JSON.parse(fs.readFileSync('tmp/simpleProductSchema.json'));//validate payload against schema - for variant and simple products
  let productSchemaOfVariant = await JSON.parse(fs.readFileSync('tmp/variantProductSchema.json'));
  const imagePrefix = s3ImageLocation + `${pgmId}/image/products/`;
  //loop through json data
  while (indexOfCsv < productCsvList.length) {
    //get indices of same product
    let indices = getIndicesOfAProduct(productCsvList, indexOfCsv);
    let productPayload = await generateProductPayload(indices, imagePrefix, productCsvList, pgmId, importGroupId);
    promiseTask.push(validateAndPush(productPayload, pgmId, importGroupId, productSchemaOfSimple, productSchemaOfVariant))

    //Increment indices
    indexOfCsv += indices.length;
  };
  let taskProcessedMsg = {
    s3Task: "TASK-COMPLETED"
  }
  //After sending the productPayloads to FIFO queue the TASK COMPLETE message is passed
  promiseTask.push(pushToQue(pgmId, taskProcessedMsg, importGroupId))
  await Promise.all(promiseTask);
  console.log("==========Product Imported==========")
  if (productsWithImportIssues.length > 0) {
    console.log("======= Product ids failed for import =======");
    productsWithImportIssues.map(productName => { console.log(productName) })
  }
}

/**
 * Product Structuring
 * @param {*} indices 
 * @param {*} imagePrefix 
 * @param {*} productCsvList 
 * @returns 
 */
async function generateProductPayload(indices, imagePrefix, productCsvList, pgmId, importGroupId) {

  let rootProductCsvItem = productCsvList[indices[0]];
  let productPayload = await setCommonProductDetails(rootProductCsvItem, pgmId, importGroupId);

  // Check If Product is SIMPLE_PRODUCT or VARIANT_PRODUCT
  if (rootProductCsvItem.productType === VARIANT_PRODUCT) {
    variantProductStructuring(rootProductCsvItem, productCsvList, productPayload, imagePrefix, indices);
  }
  else {
    simpleProductStructuring(rootProductCsvItem, productPayload, imagePrefix);
  }
  return productPayload;
}


/**
 * Set Common product details like Categorie, Brand and more fields exept Variant or Simple 
 * Product specific details
 * @param {*} rootProductCsvItem 
 * @returns 
 */
async function setCommonProductDetails(rootProductCsvItem, pgmId, importGroupId) {
  let productPayload = {};
  productPayload.images = [];
  //check categories have an value, muliple values are seprated by ;
  if (rootProductCsvItem.categories) {
    productPayload.categories = [];
    //For category id checking from db with corresponding name '?categoryName'
    if (typeof rootProductCsvItem.categories == "string" && rootProductCsvItem.categories.charAt(0) == '?') {
      //In Product category is passed has object eg: category:{id:"categoryId"}
      let category = {}
      //get category Id from category API
      category.id = await getCategoryId(pgmId, rootProductCsvItem.categories.slice(1), importGroupId)
      //if the category exist the id is pushed to the product categories
      if (category.id != "") {
        productPayload.categories.push(category)
      }
    } else {
      let categoriesUnSplitted = handleLastSemiColon(rootProductCsvItem.categories);
      let categoryIds = categoriesUnSplitted.split(';');
      categoryIds.forEach((it) => {
        let category = {};
        category.id = it;
        productPayload.categories.push(category);
      });
    }
  }
  //check if brand exists
  if (rootProductCsvItem.brand && rootProductCsvItem.brand != "") {
    if (rootProductCsvItem.brand.charAt(0) == '?') {
      let brandId = await getBrandId(pgmId, rootProductCsvItem.brand.slice(1))
      if (brandId != "") {
        productPayload.brand = {
          "id": brandId
        };
      }
    } else {
      productPayload.brand = {
        "id": rootProductCsvItem.brand
      }
    }
  }
  //assign values from csv to product payload keys
  if (rootProductCsvItem.id) {
    productPayload.id = rootProductCsvItem.id;
  }
  if (rootProductCsvItem.isFeatured) {
    productPayload.isFeatured = rootProductCsvItem.isFeatured.toLowerCase() == "true";
  }
  productPayload.isHidden = rootProductCsvItem.isHidden.toLowerCase() == "true";
  if (rootProductCsvItem.label != '') {
    productPayload.label = rootProductCsvItem.label;
  }
  //Setting the dataSource field for product payload
  if (rootProductCsvItem.dataSource != undefined && rootProductCsvItem.dataSource != "") {
    productPayload.dataSource = rootProductCsvItem.dataSource;
  }
  //setting supplierId for the product
  if (rootProductCsvItem.supplierId) {
    productPayload.supplierId = rootProductCsvItem.supplierId;
  }
  //warehouseId for the product
  if (rootProductCsvItem.warehouseId) {
    productPayload.warehouseId = rootProductCsvItem.warehouseId;
  }
  else {
    productPayload.label = "NONE";
  }
  productPayload.productType = rootProductCsvItem.productType;
  if (rootProductCsvItem.productItemType2 != '') {
    productPayload.productItemType2 = rootProductCsvItem.productItemType2;
  }
  if (rootProductCsvItem.otherProductProps != '' && rootProductCsvItem.otherProductProps != '{}') {
    productPayload.otherProductProps = JSON.parse(rootProductCsvItem.otherProductProps)
  }
  //sortPriority for product
  if(rootProductCsvItem.sortPriority && rootProductCsvItem.sortPriority!="") {
    productPayload.sortPriority  = rootProductCsvItem.sortPriority;
  }
  productPayload.name = rootProductCsvItem.name;
  productPayload.productItemType = rootProductCsvItem.productItemType;
  productPayload.searchSuggestions = rootProductCsvItem.searchSuggestion;
  productPayload.description = rootProductCsvItem.description;
  productPayload.stockManaged = rootProductCsvItem.stockManaged.toLowerCase() == "true";
  productPayload.isStockManaged = rootProductCsvItem.isStockManaged.toLowerCase() == "true";
  return productPayload;
}

/**
 * Get brandId for the brand name
 * @param {*} pgmId 
 * @param {*} brandName 
 * @returns 
 */
async function getBrandId(pgmId, brandName) {
  let getBrandApi = await config.getValueFromSSM("brandGetApi") + encodeURIComponent(brandName);
  //get brandId if the brand already exist 
  let id = await invokeBrandAPI(pgmId, "", getBrandApi, "GET", importGroupId, NEARME_TRANS_TYPE)
  //brand not exist
  if (id == null) {
    return "";
  }
  //if exist return the brandId
  return id;
}


/**
 * Get categoryId  for the category name
 * In case Category do not exist, this method will create new Category with same name
 * @param {*} pgmId 
 * @param {*} categoryName 
 * @returns 
 */
async function getCategoryId(pgmId, categoryName, importGroupId) {
  let getCategoryApi = await config.getValueFromSSM("categoryGetApi") + encodeURIComponent(categoryName);
  //get categoryId if the category already exist 
  let id = await invokeCategoryAPI(pgmId, "", getCategoryApi, "GET", importGroupId, NEARME_TRANS_TYPE)
  //category not exist in db
  if (id == null) {
    return "";
  }
  return id;
}

/**
 * Get indices of same product from the CSV file
 * @param {*} productCsvList 
 * @param {*} startIndex 
 * @returns 
 */
function getIndicesOfAProduct(productCsvList, startIndex) {
  let indices = [];
  let name = productCsvList[startIndex].name;
  indices.push(startIndex);
  while (typeof productCsvList[startIndex + 1] != 'undefined' && name === productCsvList[startIndex + 1].name) {
    indices.push(++startIndex);
  }
  return indices;
}

/**
 * Function to handle semicolon
 * @param {*} input 
 * @returns 
 */
function handleLastSemiColon(input) {
  if (input[input.length - 1] === ';') {
    return input.substring(0, input.length - 1);
  }
  return input;
}


/**
  * Properties specific to VARIANT_PRODUCT
  * @param {*} rootProductCsvItem 
  * @param {*} productPayload 
  */
function variantProductStructuring(rootProductCsvItem, productCsvList, productPayload, imagePrefix, indices) {
  // check if a main image exists for the variant product
  setRootImageOfProduct(rootProductCsvItem, imagePrefix, productPayload);
  productPayload.productType = VARIANT_PRODUCT;
  productPayload.isOutOfStock = rootProductCsvItem.SP_isOutofstock.toLowerCase() == 'true';
  //set variations
  productPayload.variations = [];
  indices.forEach(csvIndex => {
    let variantProductCsvItem = productCsvList[csvIndex];
    addVariant(productPayload, variantProductCsvItem, imagePrefix);
  })
}

/**
 * Set Variant Product details 
 * @param {*} productPayload 
 * @param {*} variantProductCsvItem 
 * @param {*} imagePrefix 
 */
function addVariant(productPayload, variantProductCsvItem, imagePrefix) {
  let variant = {};
  variant.images = [];
  productPayload.variations.push(variant);
  if (variantProductCsvItem.VP_images) {
    //remove last ;
    let imageBeforeSpltting = variantProductCsvItem.VP_images;
    imageBeforeSpltting = handleLastSemiColon(imageBeforeSpltting);
    let imageUrls = imageBeforeSpltting.split(';');
    imageUrls.forEach((urls) => {
      let correctedImageUrl = imagePrefix + urls
      let variantImages = {};
      variantImages.url = correctedImageUrl;
      variantImages.imageLocation = "S3";
      variant.images.push(variantImages);
    })
  }
  variant.salePrice = parseFloat(variantProductCsvItem.VP_salePrice);
  //setting skuId
  variant.sku = formatSku(variantProductCsvItem.VP_sku)
  //Setting default skuRule for the product
  if (variantProductCsvItem.VP_skuRule == '') {
    variant.skuRule = "1|-|0";
  } else {
    variant.skuRule = variantProductCsvItem.VP_skuRule;
  }
  variant.variantName = variantProductCsvItem.VP_name;
  variant.isOutOfStock = variantProductCsvItem.VP_isOutofstock.toLowerCase() == "true";
  variant.discountPercentage = parseFloat(variantProductCsvItem.VP_discount);
  variant.mrp = parseFloat(variantProductCsvItem.VP_mrp);
}


/**
 * properties specific to SIMPLE_PRODUCT
 * @param {*} rootProductCsvItem 
 * @param {*} productPayload 
 */
function simpleProductStructuring(rootProductCsvItem, productPayload, imagePrefix) {
  setRootImageOfProduct(rootProductCsvItem, imagePrefix, productPayload);
  //setting skuId
  productPayload.skuId = formatSku(rootProductCsvItem.SP_skuId)
  productPayload.skuRule = rootProductCsvItem.SP_skuRule;
  productPayload.salePrice = parseFloat(rootProductCsvItem.SP_salePrice);
  productPayload.mrp = parseFloat(rootProductCsvItem.SP_mrp);
  productPayload.isOutOfStock = rootProductCsvItem.SP_isOutofstock.toLowerCase() == 'true';
  productPayload.discountPercentage = parseFloat(rootProductCsvItem.SP_discount);
}


/**
 * Set root image of the product
 * @param {*} item 
 * @param {*} imagePrefix 
 * @param {*} productPayload 
 */
function setRootImageOfProduct(item, imagePrefix, productPayload) {
  if (item.SP_images) {
    let imageBeforeSplitting = item.SP_images;
    imageBeforeSplitting = handleLastSemiColon(imageBeforeSplitting);
    let imageUrls = imageBeforeSplitting.split(';');
    imageUrls.forEach((urls) => {
      let correctedImageUrl = imagePrefix + urls;
      let productImages = {};
      productImages.url = correctedImageUrl;
      productImages.imageLocation = "S3";
      productPayload.images.push(productImages);
    });
  }
}

/**
 * ProductPayload Schema validate and publish to the queue
 * @param {*} productPayload 
 * @param {*} pgmId 
 * @param {*} csvFileName 
 */
async function validateAndPush(productPayload, pgmId, csvFileName, productSchemaOfSimple, productSchemaOfVariant) {
  let productSchema = productSchemaOfSimple;
  if (productPayload.productType === VARIANT_PRODUCT) {
    productSchema = productSchemaOfVariant;
  }
  let validationResult = validator.validate(productPayload, productSchema);
  if (validationResult.errors.length > 0) {
    productsWithImportIssues.push(productPayload.name);
    let nearmeErrorCode = NEARME_APP_IDENTIFIER + "-PRODUCT-PAYLOAD-VALIDATION"
    //print validation result
    let reason = "Schema Validation Error for product:" + productPayload.name + "Validation Result:" + validationResult
    console.log(reason)
    //product validation error entry in trasLog table
    nearmeTransLog.captureErrorToTransErrorDb(productPayload.name, csvFileName, pgmId, reason, nearmeErrorCode, NEARME_TRANS_TYPE)

  };
  // insert productPayload into the SQS queue
  await pushToQue(pgmId, productPayload, csvFileName);
}

/**
 * Insert the productPayload to the Queue
 * @param {*} pgmId 
 * @param {*} messageBody 
 * @param {*} csvFileName 
 */
async function pushToQue(pgmId, messageBody, csvFileName) {
  await insertToQue(pgmId, messageBody, csvFileName)
    .then((response) => {
      console.log("Message ID=" + response.MessageId);
    }).catch(err => {
      let nearmeErrorCode = NEARME_APP_IDENTIFIER + "-PRODUCT-INSERT-QUEUE";
      //error details entry in error table
      nearmeTransLog.captureErrorToTransErrorDb(messageBody.name, csvFileName, pgmId, err.message, nearmeErrorCode, NEARME_TRANS_TYPE);
    });
}

/**
 * Removing single qoutes from skuId first character
 * @param {*} skuId 
 * @returns 
 */
function formatSku(skuId) {
  if (skuId.charAt(0) == "'") {
    return skuId.slice(1);
  }
  return skuId
}