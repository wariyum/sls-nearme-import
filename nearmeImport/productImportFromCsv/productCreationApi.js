const aws4 = require('aws4');
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const ssm = require("/opt/config.js")
let nearmeTransLog = require("/opt/transLogEntry.js")
let NEARME_ERROR_CODE = "SS_TL-PRODUCT-UPSERT_API-FAILURE"
const NEARME_TXN_TYPE = "Import-Products"
/**
 * Event insert Api is invoked to insert the product payload to Event Driven Table
 * @param {*} pgmId 
 * @param {*} productPayload 
 * @param {*} eventEntryApi 
 * @returns 
 */
module.exports = async function productCreation(pgmId, productPayload, productApi, messageGrpId) {
  const accessKey = await ssm.getValueFromSSM("accesskey");
  const secretKey = await ssm.getValueFromSSM("secretkey");
  const url = new URL(productApi);
  const params = {
    method: 'POST',
    host: url.host,
    path: url.pathname,
    service: 'execute-api',
    body: JSON.stringify(productPayload),
    region: process.env.AWS_REGION,
    headers: {
      "content-type": "application/json",
      "war-pgm": `${pgmId}`
    }
  };
  const signature = aws4.sign(params, { accessKeyId: accessKey, secretAccessKey: secretKey });
  //invoke product API and create product 
  await fetch(url, signature).then(async response => {
    let responseBody = await response.json();
    //if product created successfully log is given
    if (responseBody.success != null) {
      console.log("Created Product " + responseBody.success.name)
    }
    else {
      //product API failure error entry in trasLog table
      nearmeTransLog.captureErrorToTransErrorDb(productPayload.name, messageGrpId, pgmId, responseBody.error.errorCode, NEARME_ERROR_CODE, NEARME_TXN_TYPE)
      throw new Error(responseBody)
    }
  })
    .catch(err => {
      nearmeTransLog.captureErrorToTransErrorDb(productPayload.name, messageGrpId, pgmId, err.message, NEARME_ERROR_CODE, NEARME_TXN_TYPE)
      throw err;
    })
}
