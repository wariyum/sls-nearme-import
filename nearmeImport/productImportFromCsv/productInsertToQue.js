const { SQSClient, SendMessageCommand } = require("@aws-sdk/client-sqs");
const sqsClient = new SQSClient({ region: process.env.AWS_REGION });
const config = require('/opt/config.js');

/**
 * Product payload insert to the SQS queue
 * @param {*} pgmId 
 * @param {*} messageBody 
 * @param {*} fileName 
 * @returns 
 */
module.exports = async function productInsertToQueue(pgmId, messageBody, fileName) {
    //pgmId adding to the product payload
    messageBody.hashKey = pgmId;
    // Createl̥ an SQS service object
    const queueUrl = await config.getValueFromSSM("productUpsertQueUrl")
    let deDuplicationId = ""
    if (messageBody.s3Task != undefined && messageBody.s3Task == "TASK-COMPLETED") {
        deDuplicationId = "taskComplete"
        messageBody.fileName = fileName
    } else {
        deDuplicationId = messageBody.name.replace(/[^a-zA-Z0-9]/g, "")
    }
    let params = {
        MessageBody: JSON.stringify(messageBody),
        QueueUrl: queueUrl,
        MessageDeduplicationId: deDuplicationId,  // Required for FIFO queues
        MessageGroupId: fileName, //imorted product csv filename
    };
    return sqsClient.send(new SendMessageCommand(params))
}