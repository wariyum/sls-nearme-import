  // sample uploadedFilePath : "4-1-NER-1/import-product/product.csv";
  // sample uploadedFilePath : "4-1-NER-1/import-brand/brand.csv";
  // sample uploadedFilePath : "4-1-NER-1/import-category/category.csv";

  to run locally : 
  serverless invoke local --function s3getpresignurl --path testEvent.json

  to deploy : 
  serverless deploy


Importing Products from S3 
==============================
1. Read csv from S3 
2. Convert CSV to Product JSON
- Rows in CSV are each variant and those are combined to make a Wariyum Product.
3. Validate Product-JSON, against Product Schemas
4. Valide Product-JSON, shall be pushed to SQS - "productupsert"

SQS FIFO
=====================================
SQS FIFO queue is used to import items

Two parameters are required while sending messages to SQS FIFO queue
--------------------------------------------------------------------
1.Message deduplication ID :
    The token used for deduplication of sent messages. If a message with a particular message deduplication ID is sent successfully, any messages sent with the same message deduplication ID are accepted successfully but aren't delivered during the 5-minute deduplication interval.
2.Message group ID :
    The tag that specifies that a message belongs to a specific message group. Messages that belong to the same message group are always processed one by one, in a strict order relative to the message group (however, messages that belong to different message groups might be processed out of order).

Ref : https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/FIFO-queues-understanding-logic.html


SQS FIFO Batch Failure management
=======================================
Problem: If a message failed to invoke (thrown error) in a batch, the all messages in the batch are send to the SQS DLQ queue, both the success message and the failure message.

Solution: Only failed messages are sent to the SQS DLQ queue, after invoking the messages in the batch.

Steps:
- Exception handling should be in place and then lambda function has to return the message ids of the messages that requires reprocessing
- To enable and handle partial batch failure, check the Report batch item failures option under Additional settings while adding the SQS trigger.
- After the SQS event source configuration, the response part in the code should be in a particular format that is given below for the partial batch response failure to work.

{    
    "batchItemFailures": [          
        {             
            "itemIdentifier": "id2"         
        },         
        {             
            "itemIdentifier": "id4"         
        }     
    ] 
}

Ref: https://medium.com/srcecde/handle-sqs-message-failure-in-batch-with-partial-batch-response-b858ad212573

Data import complete status
========================================================
- Sending message "TASK COMPLETED" to SQS FIFO queue after passing the complete data list.
- {
    s3Task:"TASK COMPLETED"
  }
- SQS FIFO listen lambda will check the message is "processed" then check token exist in db . Invoke the task success to the step function.


