const brandInsertToQue = require('./brandInsertToQue.js');
const invokeBrandAPI = require('./brandCreationApi.js')
const ssm = require("/opt/config.js")
const NEARME_TXN_TYPE = "Import-Brands";
/**
 * Function for importing brands to a pgmId 
 * @param {*} brandCsvList 
 * @param {*} pgmId 
 * @returns 
 */
module.exports = async function importBrandsFromCsv(brandCsvList, pgmId, importGroupId) {
    const s3ImageLocation = await ssm.getValueFromSSM("s3imagePrefixUrl")
    const imagePrefix = s3ImageLocation + `${pgmId}/image/brands/`;
    let indexOfCsv = 0;
    let brandsPayloadForImport = [];
    // Step 1: loop through the brand JSON payload 
    console.log("IMPORT_COUNT_OF_BRAND-" + pgmId + ": " + brandCsvList.length)
    while (indexOfCsv < brandCsvList.length) {
        let brandItemInCsv = brandCsvList[indexOfCsv];
        indexOfCsv++;
        let brandPayload = {};
        if (brandItemInCsv.id && brandItemInCsv.id != "") {
            brandPayload.id = brandItemInCsv.id;
        }
        //brand find or create with brandName '?brandName'
        if (brandItemInCsv.name.charAt(0) == '?') {
            //Check the brand Name exist if not exist create new brand with this name
            let brandExist = await checkBrandExist(pgmId, brandItemInCsv.name.slice(1), importGroupId)
            //if brand exist skip the loop for creating the brand
            if (brandExist) {
                continue;
            }
            //create new brand with the name
            else {
                brandPayload.name = brandItemInCsv.name.slice(1);
            }
        } else {
            brandPayload.name = brandItemInCsv.name;
        }
        brandPayload.images = [];
        //brand payload has images
        if (brandItemInCsv.images != undefined && brandItemInCsv.images != "") {
            let imageItems = {};
            imageItems.url = imagePrefix + brandItemInCsv.images;
            imageItems.imageLocation = "S3";
            brandPayload.images.push(imageItems);
        }
        //nameHash Attribute
        brandPayload.nameHash = brandPayload.name.toLowerCase();
        //Adding the attribute priority
        if (brandItemInCsv.priority != undefined) {
            brandPayload.priority = brandItemInCsv.priority;
        }
        brandsPayloadForImport.push(brandInsertToQue(pgmId, brandPayload, importGroupId));
    };
    let taskProcessedMsg = {
        s3Task: "TASK-COMPLETED"
    }
    //After sending the brandPayloads the TASK COMPLETE message is passed
    brandsPayloadForImport.push(brandInsertToQue(pgmId, taskProcessedMsg, importGroupId))
    //payloads are pushed to the queue
    await Promise.all(brandsPayloadForImport)
    console.log("=======Brand Import Completed=======");
}


/**
 * Check the brandExist in db
 * @param {*} pgmId 
 * @param {*} brandName 
 * @returns 
 */
async function checkBrandExist(pgmId, brandName, importGroupId) {
    let getBrandApi = await ssm.getValueFromSSM("brandGetApi") + encodeURIComponent(brandName);
    //get brandId if the brand already exist 
    let id = await invokeBrandAPI(pgmId, "", getBrandApi, "GET", importGroupId, NEARME_TXN_TYPE)
    if (id != null) {
        return true;
    }
    return false;
}