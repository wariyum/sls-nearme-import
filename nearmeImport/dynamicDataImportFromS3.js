const AWS = require('aws-sdk');
const csvtojson = require('csvtojson');
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const ssm = require("/opt/config.js")
const sendToken = require("/opt/sf-taskToken/sendTaskToken.js")
const createBrand = require('./brandsImportFromCsv/brandCreationApi.js')
const createProduct = require('./productImportFromCsv/productCreationApi.js')
const importProductsFromCsv = require('./productImportFromCsv/productImportFromCsv.js')
const importBrandsFromCsv = require('./brandsImportFromCsv/brandsImportFromCsv.js')
const importCategoriesFromCsv = require('./categoryImportFromCsv/categoryImportFromCsv.js')
const createCategory = require('./categoryImportFromCsv/categoryCreationApi.js')
const bulkImportImages = require('./bulkImportImages/uploadImages.js')
const DYNAMIC_PRODUCT_UPDATE = "dyanamicproductupdate";
const IMPORT_PRODUCTS = "Import-Products";
const IMPORT_BRANDS = "Import-Brands";
const IMPORT_CATEGORIES = "Import-Categories";
const IMAGES = "Import-Images";
let nearmeTransLog = require("/opt/transLogEntry.js")

/** 
 * function to invoke the bulk update API
 * @param {*} pgmId 
 * @param {*} key 
 * @returns 
 */
async function bulkUpdateProducts(pgmId, csvFileName) {
  const dynamicProductImportApi = await ssm.getValueFromSSM("dynamicProductImport-api");
  return await fetch(dynamicProductImportApi + csvFileName + "?" + "pgmId=" + `${pgmId}`, {
    method: 'POST',
    headers: {
      "content-type": "application/json",
      "war-pgm": `${pgmId}`,
    },
    body: {}
  });
}
/**
 * Lambda Function to invoke data importing and product properties updation for nearme task based on the event triggered
 * @param {*} event 
 */
module.exports.dynamicDataImport = async (event) => {
  // Aws ssm to fetch bucket name and configuration details
  const s3BucketName = await ssm.getValueFromSSM("dataImportS3Bucket");
  const awsRegion = process.env.AWS_REGION;
  const accesskey = await ssm.getValueFromSSM("accesskey");
  const secretKey = await ssm.getValueFromSSM("secretkey");
  const s3Config = { accessKeyId: accesskey, secretAccessKey: secretKey, region: awsRegion }
  AWS.config.update({ region: awsRegion, });
  let s3UploadedFilePath = null;
  //fetching uploaded file path from event
  const result = event.Records;
  result.forEach(result => {
    if (result.s3.object.key) {
      //sample uploadedFilePath : "pgmId/dynamicProductUpdate/product.csv";
      s3UploadedFilePath = result.s3.object.key;
    }
  });
  const pgmId = getPathVariables(s3UploadedFilePath, 0)
  //uploaded product Csv filename
  //Setting up importing file name as SQS-FIFO GroupId - for unique importing of items in a batch
  const importedFileName = getPathVariables(s3UploadedFilePath, 2)
  // assign the key value to check the event source :- dynamicproductupdate/import-product/import-brands/import-categories
  const s3EventKey = getPathVariables(s3UploadedFilePath, 1)
  // perform operation based on the case
  switch (s3EventKey) {
    // Perform Third Party Dynamic Property Update
    case DYNAMIC_PRODUCT_UPDATE:
      console.log("The event is : " + DYNAMIC_PRODUCT_UPDATE + " for pgmId : " + pgmId);
      //transactional log entry in db for third party sync
      nearmeTransLog.logTxnEntry("uploaded", importedFileName, pgmId, "Uploaded file :" + importedFileName, "Sync-Third-Party-Inventory", "started")
      const dyanamicproductupdate = await bulkUpdateProducts(pgmId, importedFileName);
      console.log("===== Response ====== ", dyanamicproductupdate);
      break;
    // Case : Perform Product Importing
    case "import-product":
      console.log("The event is :" + IMPORT_PRODUCTS + " for pgmId :" + pgmId);
      //transactional log entry in db for importing products
      nearmeTransLog.logTxnEntry("uploaded", importedFileName, pgmId, "Uploaded file :" + importedFileName, IMPORT_PRODUCTS, "started")
      //Read Uploaded csv file data from S3 and convert to JSON
      const productImportPayload = await getCsvToJsonDataFromS3(s3BucketName, s3UploadedFilePath, s3Config);
      //   invoking the product import function
      await importProductsFromCsv(productImportPayload, pgmId, importedFileName);
      break;
    // Case : Perform Brand Importing
    case "import-brand":
      console.log("The event is :" + IMPORT_BRANDS + " for pgmId :" + pgmId);
      //transactional log entry in db for importing brands
      nearmeTransLog.logTxnEntry("uploaded", importedFileName, pgmId, "Uploaded file :" + importedFileName, IMPORT_BRANDS, "started")
      //Read Uploaded csv file data from S3 and convert to JSON
      const brandImportPayload = await getCsvToJsonDataFromS3(s3BucketName, s3UploadedFilePath, s3Config);
      // invoking the brand import function
      await importBrandsFromCsv(brandImportPayload, pgmId, importedFileName);
      break;
    //Case : Perform Category Importing
    case "import-category":
      console.log("The event is :" + IMPORT_CATEGORIES + " for pgmId :" + pgmId);
      //transactional log entry in db for importing categories
      nearmeTransLog.logTxnEntry("uploaded", importedFileName, pgmId, "Uploaded file :" + importedFileName, IMPORT_CATEGORIES, "started")
      //Read Uploaded csv file data from S3 and convert to JSON
      const categoryImportPayload = await getCsvToJsonDataFromS3(s3BucketName, s3UploadedFilePath, s3Config);
      // invoking the category import function
      await importCategoriesFromCsv(categoryImportPayload, pgmId, importedFileName);
      break;
    case "import-images":
      console.log("The event is :" + IMAGES + " for pgmId :" + pgmId);
      //imageItemType: products, categories ,brands... etc...
      const imageItemType = getPathVariables(s3UploadedFilePath, 2);
      const imageFileName = getPathVariables(s3UploadedFilePath, 3)
      //transactional log entry in db for importing images
      nearmeTransLog.logTxnEntry("uploaded", imageFileName, pgmId, "Uploaded " + imageItemType + "file :" + importedFileName, IMAGES, "started")
      //Read from Csv to json
      const imageCsvList = await getCsvToJsonDataFromS3(s3BucketName, s3UploadedFilePath, s3Config);
      //Bulk importing the images
      await bulkImportImages(imageCsvList, pgmId, imageItemType, importedFileName)
      //Transactional log entry for completing the image uploads
      await nearmeTransLog.logTxnEntry("Import_Completed", imageFileName, pgmId, imageItemType + " Images Import Process Completed", IMAGES, "completed")
      break;
    default:
      console.log("Event Type not found")
  }
}

/**
 * Product payload read from SQS queue and invoke the api to create the product
 * @param {*} event 
 */
module.exports.productInsertFromQue = async (event) => {
  let messagesToReprocess = []
  let eventConfiguration = event.Records
  //creation API
  let productApi = await ssm.getValueFromSSM("productCreationApi")
  await Promise.all(eventConfiguration.map(async messages => {
    let productPayload = await JSON.parse(messages.body)
    let pgmId = productPayload.hashKey
    let messageId = messages.messageId
    let messageGrpId = messages.attributes.MessageGroupId
    //For sending succes message to task step fuction 
    if (productPayload.s3Task == "TASK-COMPLETED") {
      //trans log entry for product import completion
      return nearmeTransLog.logTxnEntry("Import_Completed", messageGrpId, pgmId, "Products Import Process Completed", IMPORT_PRODUCTS, "completed")

    }
    //Invoke the api call to insert product
    try {
      await createProduct(pgmId, productPayload, productApi, messageGrpId)
    }
    catch (err) {
      //failed productPayload messageId is append messagesToProcess
      messagesToReprocess.push({ "itemIdentifier": messageId })
    }
  }))
  //Partial batchFailureResponse is returned to SQS queue
  return getBatchFailureResponse(messagesToReprocess)
}

/**
 * Brand payload read from SQS queue and invoke the api to create the brand
 * @param {*} event 
 */
module.exports.brandInsertFromQue = async (event) => {
  let messagesToReprocess = []
  let eventConfiguration = event.Records
  const brandCreationApi = await ssm.getValueFromSSM("brandCreationApi");
  await Promise.all(eventConfiguration.map(async messages => {
    let messageBody = await JSON.parse(messages.body)
    let pgmId = messageBody.hashKey
    let messageGrpId = messages.attributes.MessageGroupId
    //For sending succes message to task step fuction 
    if (messageBody.s3Task == "TASK-COMPLETED") {
      let id = "brand_import#" + messageBody.fileName
      //trans log entry for product import completion
      nearmeTransLog.logTxnEntry("Import_Completed", messageGrpId, pgmId, "Brands Import Process Completed", IMPORT_BRANDS, "completed")
      //Invoke the stepFunction task success for brand import
      return await sendToken.sendTaskSuccess(pgmId, id, messageBody)
    }
    //Invoke the api call to insert brand
    try {
      await createBrand(pgmId, messageBody, brandCreationApi, "POST", messageGrpId, IMPORT_BRANDS)
    }
    catch (err) {
      let messageId = messages.messageId
      //failed productPayload messageId is append messagesToProcess
      messagesToReprocess.push({ "itemIdentifier": messageId })
    }
  }))
  //Partial batchFailureResponse is returned to SQS queue
  return getBatchFailureResponse(messagesToReprocess)
};

/**
 * Category payload read from SQS queue and invoke the api to create the Category
 * @param {*} event 
 */
module.exports.categoryInsertFromQue = async (event) => {
  let messagesToReprocess = []
  let eventConfiguration = event.Records
  const categoryCreationApi = await ssm.getValueFromSSM("categoryCreationApi");
  await Promise.all(eventConfiguration.map(async messages => {
    let messageBody = await JSON.parse(messages.body)
    let pgmId = messageBody.hashKey
    let messageGrpId = messages.attributes.MessageGroupId
    //For sending succes message to task step fuction 
    if (messageBody.s3Task == "TASK-COMPLETED") {
      let id = "category_import#" + messageBody.fileName
      //trans log entry for product import completion
      nearmeTransLog.logTxnEntry("Import_Completed", messageGrpId, pgmId, "Categories Import Process Completed", IMPORT_CATEGORIES, "completed")
      //Invoke the stepFunction task success for brand import
      return await sendToken.sendTaskSuccess(pgmId, id, messageBody)
    }
    //Invoke the api call to insert category
    try {
      await createCategory(pgmId, messageBody, categoryCreationApi, "POST", messageGrpId, IMPORT_CATEGORIES)
    }
    catch (err) {
      let messageId = messages.messageId
      //failed productPayload messageId is append messagesToProcess
      messagesToReprocess.push({ "itemIdentifier": messageId })
    }
  }))
  //Partial batchFailureResponse is returned to SQS queue
  return getBatchFailureResponse(messagesToReprocess)
};

// function to get csv file from s3 and convert data to json
async function getCsvToJsonDataFromS3(s3BucketName, s3UploadedFilePath, s3Config) {
  var params = { Bucket: s3BucketName, Key: s3UploadedFilePath }
  var s3 = new AWS.S3(s3Config);
  const stream = s3.getObject(params).createReadStream();
  const csvDataToJson = await csvtojson().fromStream(stream);
  return csvDataToJson;
}
// function to get path variables from full  path name
const getPathVariables = function (pathname, index) {
  let pathVariable = pathname.split('/');
  return pathVariable[index]
}

/**
 * Get batch failure message Response
 * @param {*} messagesToReprocess 
 * @returns 
 */
function getBatchFailureResponse(messagesToReprocess) {
  let batchFailureResponse = {}
  batchFailureResponse["batchItemFailures"] = messagesToReprocess
  console.log(batchFailureResponse)
  return batchFailureResponse
}