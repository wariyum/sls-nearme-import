const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const ssm = require("/opt/config.js")
const AWS = require("aws-sdk")
let nearmeTransLog = require("/opt/transLogEntry.js")
const NEARME_ERROR_CODE = "SS_TL-IMAGE-UPLOAD-FAILURE"
const NEARME_TXN_TYPE = "Import-Images"


/**
 * Read the ImageUrl csv file, Invoke the url and upload the image to S3
 * @param {*} imageCsvList 
 * @param {*} pgmId 
 * @param {*} imageItemType 
 */
module.exports = async function bulkImportImages(imageCsvList, pgmId, imageItemType, importedFileName) {
    console.log("IMAGE LIST COUNT :" + imageCsvList.length)
    //loop through the list of csvItem
    try {
        await Promise.all(imageCsvList.map(async imageData => {
            await uploadImagesToS3(imageData, pgmId, imageItemType, importedFileName)
        }))
    } catch (e) {
        console.error(e)
    }
}

/**
 * Invoking the image url and upload image tothe S3 Bucket
 * @param {*} imageData 
 * @param {*} pgmId 
 * @param {*} imageItemType 
 */
async function uploadImagesToS3(imageData, pgmId, imageItemType, importedFileName) {
    //S3 bucket in which the image is uploaded
    const s3BucketName = await ssm.getValueFromSSM("nearmeImageBucket");
    const s3Oject = new AWS.S3({ region: process.env.AWS_REGION })
    const url = imageData.imageUrl;
    //Invoking the imageUrl
    await fetch(url).then(async response => {
        if (response.status != 404) {
            //response is changed to buffer type to upload to S3
            const imageArrayBuffer = await response.arrayBuffer();
            const imageBuffer = Buffer.from(imageArrayBuffer);
            //Default : fileName is taken from url 
            let fileName = url.split("/").pop()
            //Checking the suggestedImageName exist and it is not empty
            if (imageData.suggestedImageName != undefined && imageData.suggestedImageName != '') {
                //The suggested name is given in Csv the fileName is change to suggested name
                fileName = imageData.suggestedImageName;
            }
            //uploading the image to S3 bucket
            //path:demowariyum/{pgmId}/image/products/
            await s3Oject.upload({
                Bucket: s3BucketName + "/" + pgmId + "/image/" + imageItemType,
                Key: fileName,
                Body: imageBuffer,
                ACL: 'public-read'
            }).promise().then(result => {
                console.log(fileName + " uploaded")
            }).catch(error => { throw error }); //Error thrown for uploading the file
        } else {
            let errMsg = "No Image Found for " + imageItemType + " :" + fileName
            //Transactional error entry for imageUrl 
            nearmeTransLog.captureErrorToTransErrorDb(fileName, importedFileName, pgmId, errMsg, NEARME_ERROR_CODE, NEARME_TXN_TYPE)
            console.log(errMsg);
        }
    }).catch(error => {
        nearmeTransLog.captureErrorToTransErrorDb(url, importedFileName, pgmId, error.message, NEARME_ERROR_CODE, NEARME_TXN_TYPE)
        console.error(error)
    }) //Url Fetch error is thrown

}

