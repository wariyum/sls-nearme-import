const csvtojson = require("csvtojson");
const jsonToCsv = require('json-2-csv')
const fs = require('fs')
const AWS = require("aws-sdk")
const dynamoDB = new AWS.DynamoDB.DocumentClient({ region: "us-east-2" })
const SKU_MARKER = "'"

/**
 * generate csv with file with field suggestedSkuId that is read from db based on EANCode
 * @param {*} csvFilePath 
 * @param {*} pgmId 
 */
function mapEanCodeToSKU(csvFilePath, pgmId) {
    csvtojson()
        .fromFile(csvFilePath).then(async productItemList => {
            //new product list with field suggestedSKU-id
            let newProductList = await Promise.all(productItemList.map(async productItem => {
                //suggested SkuId for simple product
                if (productItem.SP_skuId && productItem.SP_skuId != '') {
                    //get corresponding skuId from DB
                    productItem['suggestedSKU-ID'] = SKU_MARKER + await getSkuFromDb(productItem.SP_skuId, pgmId)
                }
                //suggested skuId for variant product
                if (productItem.VP_sku && productItem.VP_sku != '') {
                    productItem['suggestedSKU-ID'] = SKU_MARKER + await getSkuFromDb(productItem.VP_sku, pgmId)
                }
                return productItem
            }))
            //write to Csv
            await jsonToCsv.json2csvAsync(newProductList).then(productItem => {
                let fileName = "csvWithSuggestedSku.csv"
                fs.writeFileSync(fileName, productItem)
                console.log("Created File - " + fileName)
            })
        })
}
//TODO : remove this when we  change to lambda
// mapEanCodeToSKU('1productList10-01-2023.csv',"5-1-NER-1")

/**
 * generate new Csv with the corressponding skuId
 */
function getMappedSkuCsv(csvFilePath) {
    csvtojson()
        .fromFile(csvFilePath).then(async productItemList => {
            let newProductList = productItemList.map((productItem) => {
                //skuId setting for simple product from suggestedSku-Id
                if (productItem.productType == 'SIMPLE_PRODUCT' && productItem['suggestedSKU-ID'] != '') {
                    productItem.SP_skuId = productItem['suggestedSKU-ID'];
                    productItem.isStockManaged = 'TRUE'
                }
                //skuId setting for variant product from suggestedSku-Id
                if (productItem.productType == 'VARIANT_PRODUCT' && productItem['suggestedSKU-ID'] != '') {
                    productItem.VP_sku = productItem['suggestedSKU-ID'];
                    productItem.isStockManaged = 'TRUE'
                }
                //deleting suggestedSKU-ID field
                delete productItem['suggestedSKU-ID']
                return productItem;
            })
            //write to Csv
            await jsonToCsv.json2csvAsync(newProductList).then(productItem => {
                let fileName = "newProductImport.csv"
                fs.writeFileSync(fileName, productItem)
                console.log("Product File Genereated with corressponding skuId")
            })

        })

}
//TODO : remove this when we  change to lambda
getMappedSkuCsv('csvWithSuggestedSku.csv')

/**
 * get skuId from DB
 */
function getSkuFromDb(eanId, pgmId) {
    return dynamoDB.get({
        TableName: "NEARME-EANCODES",
        Key: {
            hashKey: pgmId + "#SkuBarcode",
            eanCode: eanId
        },
    })
        .promise()
        .then(eanItem => {
            //eanId is not found in db return '' else return skuId
            if (JSON.stringify(eanItem) == '{}') {
                return ''
            } else {
                return eanItem.Item.skuId
            }
        })
        .catch(console.error)
}

// getSkuFromDb("123")