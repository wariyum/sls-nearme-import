const csvtojson = require("csvtojson");
const AWS = require("aws-sdk")
const dynamoDB = new AWS.DynamoDB.DocumentClient({ region: "us-east-2" })
const csvFilePath = 'infiData.csv'

/**
 * insert eanData and sku to dynamodb from csv
 */
async function insertToDynamoDB() {
    let count =0;
    csvtojson()
        .fromFile(csvFilePath)
        .then((infiData) => {
            console.log(infiData.length);
            infiData.forEach(item => {
                let eanData = {}
                if (item.EANCode && item.EANCode != '') {
                    eanData.hashKey = "5-1-NER-1#SkuBarcode"
                    eanData.pgmId = "5-1-NER-1"
                    eanData.eanCode = item.EANCode;
                    eanData.skuId = item.ItemCode
                    //write data to db
                    dynamoDB
                        .put({
                            Item: eanData,
                            TableName: "NEARME-EANCODES",
                        })
                        .promise().then(result => {
                            count++;
                            console.log(count+" "+result)
                        }).catch(error => { console.error(error) })
                }

            })

        })

}
insertToDynamoDB(csvFilePath)



