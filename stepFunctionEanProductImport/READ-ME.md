User will upload EAN CSV file to S3 bucket

S3 bucket with Lambda trigger would trigger this import Step function with below payload
{
    eanFilePath:"s3://..../eanFileName.csv"
}

## Generate-CSVs-Of-Imports

Developer Note: Invoking Step function ref: https://stackoverflow.com/a/66950614/663742

this will generate multiple CSV files
- Products
- Brands
- Categories
- Images

and returns a JSON Payload of below format
{
   "sourceS3Path": {
      "productListPath": [
        "nearme-ean-import/4-1-NER-1/product/1productList3-16-2023T23-45.csv"
      ],
      "imageListPath": [
        "nearme-ean-import/4-1-NER-1/images/1imageList3-16-2023T23-45.csv"
      ],
      "brandListPath": [
        "nearme-ean-import/4-1-NER-1/brand/1brandList3-16-2023T23-45.csv"
      ],
      "categoryListPath": [
        "nearme-ean-import/4-1-NER-1/category/1categoryList3-16-2023T23-45.csv"
      ]
    },
    "destinationPath": {
      "productListPath": "nearme-import/4-1-NER-1/import-product",
      "imageListPath": "nearme-import/4-1-NER-1/import-images/products",
      "brandListPath": "nearme-import/4-1-NER-1/import-brand",
      "categoryListPath": "nearme-import/4-1-NER-1/import-category"
    }
  }

# Developer References

- https://docs.aws.amazon.com/toolkit-for-vscode/latest/userguide/building-stepfunctions.html#building-stepfunctions-code-snippets

- https://docs.aws.amazon.com/step-functions/latest/dg/concepts-states.html

# Task Waiting for User Input or Third Party APIS

Ref: https://instil.co/blog/step-functions-and-task-tokens/

## AWS CLI command to mark task sucessful
aws stepfunctions send-task-success --task-token $taskToken --task-output '{"result":1}'


## AWS CLI command to mark task Failed
aws stepfunctions send-task-failure --task-token $taskToken --error "DummyError" \
		--cause "Something went wrong..."

## Migrating aws-sdk package

    Using 'codemod' we can migrate aws-sdk v2 to v3 with the following command 
    $ npx aws-sdk-js-codemod -t v2-to-v3 PATH...

Ref: https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/migrating-to-v3.html

## StepFunction map state

Ref: https://github.com/awsdocs/aws-step-functions-developer-guide/blob/master/doc_source/tutorial-creating-map-state-machine.md