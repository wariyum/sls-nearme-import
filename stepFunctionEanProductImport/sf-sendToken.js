let {
  SFN: StepFunctions
} = require("@aws-sdk/client-sfn");
const stepFunctions = new StepFunctions({ region: "us-east-2" })
/**
 * For sending task success to the step function with taskToken
 * @param {*} taskToken 
 * @param {*} output 
 */
async function sendTaskSuccess(taskToken, output) {
  await stepFunctions.sendTaskSuccess({
    taskToken,
    output: JSON.stringify({
      Payload: output
    })
  });
}
//usage 
// sendTaskSuccess("AQB4AAAAKgAAAAMAAAAAAAAAAtG/XOCBe1G3Wcrbi256YUThNUQBCI7C99xE7mUeTiAAeCC/l/Az3XthtjbATJ2YKeuG+f6ToYlRbcBLM6/+fggAajpElWV4Bwn7X5lSIoHaRLOgr4k9GeLaYka13hZkbm671F23N9FmPZiY0AwQ12lrEchUgaawM7eC4SUoiz0HKg7F75qlsaBtRgFkZwSgrJVYOkJl/+XFrsIVoHSqdAxbIBoZXgt0ttWie/jM/SrVJO+aW5WSmLbx6Qgx+VpJKWlrkG3StMbr1E+h5DwbBDBbAp0VFQfGNA4U5nJJD6EjtUiTl8AWWXA6kMrmgL65QN6Fsmg8miGguJztudZzBjl9mVYPCB134Aol+OJ7Dttv8NGbIsGr2t7TZ/59g0U/iLfgj3Z+Tj23HNZqQVrN4cOAegzfsZvX2vGHm/7julEQrRor4aODnPLvJ3DuGJL6zZszGurQrpmxCdbvDxgr/k6Zc8938jRjl4a+A7UvCI3hvvZmUzPHAGLUGDT7BQcMZMVHPTIWS/I0QcDG+fm65srys0GPudsxniclt6pOKwxZXE1MzB9PqoXBBiRc/AYnhh1RZ6d8U899JMSFwLUMOoTCAkywx81s314nNVQsaj5+G135s+g/qcEw", {"brandImportStatus": "successfull"});
/**
 * send task failure with taskToken to the step function
 * @param {*} taskToken 
 * @param {*} error 
 * @param {*} cause 
 */
async function sendTaskFailure(taskToken, error, cause) {
  await stepFunctions.sendTaskFailure({
    taskToken,
    error,
    cause
  });
}
//usage 
sendTaskFailure("AQB4AAAAKgAAAAMAAAAAAAAAAmsXYh1W9drMbOv1PTu7ROCqje9mb0GRE+GPWbf5/PZxTA7Aj1ZwuCgcGLq46+2KWduwwzRH+xFLwXRnpVxWRITSRSsPHzWu9ZgkkobNEedGys9FFOsUBQZsOUGbe3RWq3VdoQoef+jGQhZJZeUCQlpgyduCVL8KVYyNblH0X6CS3e8/AE2shpm/pPMSmQ2UxuiHxBaU7x9k9K5wkcyqibX08FRCkndi/Zcdn0QbcjqUwz7zn8Dz1Pmx3PZ13bB0HQ48v18FDNjnYwGrjKbBJCPp7Z8ogYxMp+eRFSW88TgNtHFcJy1eUB2S7g4147EEr+uDiE6wSCAGpdZ1jh/ErtEAyfOK0LcvB/38Ike/1vsFzNiDXhvBeXTWt6u271+OiTFtFAsB5RUcEkH0FXJV8QB4z764QH2AbSzuwSxyTUhz9UpgXFnwCmLbHQL5wekiRT1v4OGmXNUehuSBDqpD3TRr89Pz2Nsh6qEJOYrUmu+vtyGzQvKDRlcVhMwgSWQ3wue7CJ8La7Tz28n+nfCYHxtb7DvyuJeFoX90suMdzOxC+570ngeh4H5pHgy4sz1AuxNHRto82e0XzIYPztmrEmypQK08wd4tGsid9AF2iNROZesUsi6DbQjF", "Error-test", "Something went wrong...");
