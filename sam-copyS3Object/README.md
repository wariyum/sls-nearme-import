# Lambda for copying file from S3 bucket to another bucket
===================================================================

## Implementation:

Sample: 
- source Path:"learn-bucket-urls/Reconciliation.csv"
- destinatoin Path : "nearme-import/reconciliation"

Copy the file "Reconcilaition.csv"  from "learn-bucket-urls" S3 bucket to the destination bucket "nearme-import/reconciliation/". 

//nearme-ean-import/{pgmId}/products/{product.csv} ====> //nearme-import/{pgmId}/import-product/

## Step function -

- Brand-Import-Lambda-Aync:
-------------------------------
    input: {
        filePath: {
                "brandCsvFilePath.$": "..............brandsFilePath",
                "categoryCsvFilePath.".............categoryFilePath",
                "productCsvFilePath.$": ".............productFilePath",
                "imageCsvFilePath.$": "................imageFilePath"
        },
        sourceFile: "..............brandsFilePath",
        destinationFilePath:"nearme-imports/{pgmid}/import-brands/---.brand.csv"
    }


//Fetch token and save in db
-----------------------------------------

taskToken="maidfikmdfiikdof..........................."

Dynamodb : Bucket table

PK: {pgmId}#{csvType}_import

PK: {pgmId}#STEP-TASK-TOKEN  
SK: {csvType}_import#{csvFileName}
data: {
taskToken: {token}
}

eg: {
    hashKey:4-1-NER-1#STEP-TASK-TOKEN  
    id: brand_import#brandList2023-01-02-12-9.csv
    data:{
        taskToken:"maidfikmdfiikdof..........................."
    }
}

//saveTaskToken