const { S3 } = require("@aws-sdk/client-s3");
// let awsConfig = { "region": process.env.AWS_REGION };
// AWS.config.update(awsConfig);
const s3 = new S3({ "region": process.env.AWS_REGION });
const saveToken = require("/opt/sf-taskToken/saveTokenInDb.js")
const sendToken = require("/opt/sf-taskToken/sendTaskToken.js")


/**
 * Lambda copying S3 object from S3 bucket
 * @param {*} event 
 * @param {*} context 
 * @returns 
 */
module.exports.copyS3Object = async (event, context) => {
    const token = event.taskToken;
    let destinationBucketPath = event.destinationS3Path
    let sourceS3Path = event.sourceS3Path
    const pgmId = destinationBucketPath.split('/')[1]
    let csvType = destinationBucketPath.split("/")[2]
    try {
        if (sourceS3Path == "") {
            console.log("Source file is empty")
            if (token != undefined) {
                let hashKey = pgmId + "#STEP-TASK-TOKEN"
                let id = csvType + "#MISSING"
                let output = {
                    pgmId,
                    s3Task: "COMPLETED"
                }
                //save task token details in db with initial status has PENDING
                return saveToken.saveTaskToken(hashKey, id, token, "PROCESSED").then(() => {
                    return sendToken.sendTaskSuccess(pgmId, id, output)
                })
            } else {
                return "source file is empty"
            }
        }
        //if token exist - save token details in db
        if (token != undefined) {
            //sourceS3Path: nearme-ean-import/4-1-NER-1/_processed/brand/brandList2023-01-02-12-9.csv
            //hashKey:  {pgmId}#STEP-TASK-TOKEN 
            let hashKey = pgmId + "#STEP-TASK-TOKEN"
            let csvType = sourceS3Path.split("/")[3]
            let csvFilename = sourceS3Path.split("/").pop()
            //id: {csvType}_import#{csvFileName}
            let id = csvType + "_import#" + csvFilename
            //save task token details in db with initial status has PENDING
            await saveToken.saveTaskToken(hashKey, id, token, "PENDING")
        }
        let fileName = sourceS3Path.split("/").pop()
        //to get the path to upload file : nearme-import/{pgmId}/products=> {pgmId}/products/product.csv
        let key = destinationBucketPath.substring(destinationBucketPath.indexOf("/") + 1) + "/" + fileName
        //destination s3 bucket name
        let destinationBucket = destinationBucketPath.split("/").shift()
        const params = {
            Bucket: destinationBucket,
            CopySource: sourceS3Path,
            Key: key,
        };
        return s3.copyObject(params);
    } catch (err) {
        console.error(err);
        return err;
    }
};
