const { DynamoDB } = require('@aws-sdk/client-dynamodb')
const { marshall, unmarshall } = require("@aws-sdk/util-dynamodb")
const dynamoDB = new DynamoDB({ region: process.env.AWS_REGION })

/**
 * Get skuId from db
 * @param {*} eanId 
 * @param {*} pgmId 
 * @returns 
 */
module.exports.getSkuFromDb = async (eanId, pgmId) => {
    let eanItem = await dynamoDB.getItem({
        TableName: "NEARME-EANCODES",
        Key: marshall({
            hashKey: pgmId + "#SkuBarcode",
            eanCode: eanId
        }),
    })
    //eanId is not found in db return eanId itseilf else return skuId
    if (eanItem.Item == undefined) {
        return eanId
    } else {
        return unmarshall(eanItem.Item).skuId
    }
}