const jsonToCsv = require('json-2-csv')
const fs = require('fs')
const { S3Client, PutObjectCommand } = require("@aws-sdk/client-s3");
let s3Object = new S3Client({ region: process.env.AWS_REGION });
const { DynamoDB } = require('@aws-sdk/client-dynamodb')
const { marshall, unmarshall } = require("@aws-sdk/util-dynamodb")
let dynamoDB = new DynamoDB({ region: process.env.AWS_REGION })
const ssm = require("/opt/config.js")
const FIND_OR_CREATE_MARKER = '?'
const SKU_MARKER = "'"
const s3BucketName = "nearme-ean-import"
const SPLIT_FILE_SIZE = 500
let nearmeTransLog = require("/opt/transLogEntry")
let getMappedSkuId = require("./getMappedSku")
const NEARME_TRANS_TYPE = "Import-EAN"

/**
 * PROCESS-2 in import from EAN Details
 * Generate import file with the eanCode and skuID CSV list has a VARIANT PRODUCT
 * @param {*} pgmId 
 */
module.exports.generateImportFilesForEAN = async (pgmId, productCsvList, csvFileName, productImportConfig) => {
    let productList = [], imageList = [], brandList = [], categoryList = [];
    //set created for checking duplicate names
    let uniqueBrandNameList = new Set(), uniqueCategoryNameList = new Set();
    const eanDB = await ssm.getValueFromSSM("EAN_table");
    //for validating eanCode
    const regExp = new RegExp("^[0-9]{13}$")
    //loop through the list
    await Promise.all(productCsvList.map(async csvItem => {
        //create product with Nearme export format
        let newProductDetails = {}, newImageDetails = {}, newBrandDetails = {}, newCategoryDetails = {};
        if (regExp.test(csvItem.eanCode)) {
            //Get Details of product from EAN code
            let eanProductDetails = await getEANProductDetails(Number(csvItem.eanCode), eanDB)
            //eanProduct List
            if (eanProductDetails != undefined || eanProductDetails != null) {
                //setting properties for import product file
                newProductDetails.id = "";
                newProductDetails.name = eanProductDetails.product.name;
                //Brand setting, if brand exist marked with '?{brandName}'
                if (eanProductDetails.product.brand != "" && eanProductDetails.product.brand != null) {
                    //brand property setting in product object 
                    newProductDetails.brand = FIND_OR_CREATE_MARKER + eanProductDetails.product.brand
                    //checking the brand name is already exist in the brand List
                    if (!uniqueBrandNameList.has(eanProductDetails.product.brand)) {
                        //brand property setting in brand object
                        newBrandDetails.id = "";
                        newBrandDetails.name = FIND_OR_CREATE_MARKER + eanProductDetails.product.brand
                        newBrandDetails.images = "";
                        newBrandDetails.priority = "";
                        //adding the brand name to the unique brandList
                        uniqueBrandNameList.add(eanProductDetails.product.brand)
                    }
                } else {
                    newProductDetails.brand = "";
                }
                //Category setting, if category exist marked with '?{categoryName}'
                if (eanProductDetails.product.category != "" && eanProductDetails.product.category != null) {
                    //category property setting in product object
                    newProductDetails.categories = FIND_OR_CREATE_MARKER + eanProductDetails.product.category
                    if (!uniqueCategoryNameList.has(eanProductDetails.product.category)) {
                        //category property setting in category object
                        newCategoryDetails.id = ""
                        newCategoryDetails.name = FIND_OR_CREATE_MARKER + eanProductDetails.product.category
                        newCategoryDetails.description = ""
                        newCategoryDetails.isHidden = ""
                        newCategoryDetails.priority = ""
                        newCategoryDetails.images = ""
                        newCategoryDetails.hasCategory = ""
                        newCategoryDetails.hasProduct = ""
                        //Setting parentId from the Nearme Configuration
                        if (productImportConfig.parentId != undefined) {
                            newCategoryDetails.parentId = productImportConfig.parentId;
                        } else {
                            newCategoryDetails.parentId = ""
                        }
                        //adding the category name to the unique categoryList
                        uniqueCategoryNameList.add(eanProductDetails.product.category)
                    }
                } else {
                    newProductDetails.categories = "";
                }
                //product description setting
                if (eanProductDetails.product.description == "No description found.") {
                    newProductDetails.description = "";
                } else {
                    newProductDetails.description = eanProductDetails.product.description;
                }
                newProductDetails.productType = "VARIANT_PRODUCT";
                newProductDetails.SP_skuId = "";
                newProductDetails.SP_salePrice = "";
                newProductDetails.SP_mrp = "";
                newProductDetails.SP_images = "";
                //Checking the imageUrl null in EAN product details
                if (eanProductDetails.product.imageUrl != "null") {
                    //Getting the image file name from image Url for product import file
                    let imageFileName = eanProductDetails.product.imageUrl.split("/").pop();
                    newProductDetails.SP_images = imageFileName + ";"
                    //properties setting for import image File
                    newImageDetails.imageUrl = eanProductDetails.product.imageUrl
                    newImageDetails.suggestedImageName = "";
                    imageList.push(newImageDetails);
                }
                newProductDetails.productItemType = "SUPERMARKET"
                newProductDetails.productItemType2 = ""
                newProductDetails.otherProductProps = ""
                newProductDetails.isHidden = false
                newProductDetails.isFeatured = false
                newProductDetails.label = "NONE";
                newProductDetails.searchSuggestion = "";
                newProductDetails.SP_discount = "";
                newProductDetails.SP_isOutofstock = true;
                newProductDetails.SP_skuRule = "";
                newProductDetails.stockManaged = false;
                newProductDetails.isStockManaged = false;
                newProductDetails.supplierId = "";
                newProductDetails.wareHouseId = "";
                //Setting dataSource from the Nearme Configuration
                if (productImportConfig.dataSource != undefined) {
                    newProductDetails.dataSource = productImportConfig.dataSource
                } else {
                    newProductDetails.dataSource = "";

                }
                newProductDetails.VP_name = ".";
                newProductDetails.VP_discount = "";
                newProductDetails.VP_isOutofstock = ""
                //Setting the corresspponding mapped sku From db
                newProductDetails.VP_sku = SKU_MARKER + await getMappedSkuId.getSkuFromDb(csvItem.eanCode, pgmId)
                newProductDetails.VP_salePrice = 0;
                newProductDetails.VP_mrp = 0;
                newProductDetails.VP_skuRule = "";
                newProductDetails.VP_images = "";
                productList.push(newProductDetails);
                //check the new brand Details exist
                if (Object.keys(newBrandDetails).length != 0) {
                    brandList.push(newBrandDetails);
                }
                //check category property exist
                if (Object.keys(newCategoryDetails).length != 0) {
                    categoryList.push(newCategoryDetails);
                }
            }
            else {
                //Error details save in db for eanCode productDetails is null
                nearmeTransLog.captureErrorToTransErrorDb(csvItem.eanCode, csvFileName, pgmId, "No data found in db", "WF_EANI-EANCODE-MISSING", NEARME_TRANS_TYPE)
            }
        }
        else {
            //eanCode failed validation save in DB
            nearmeTransLog.captureErrorToTransErrorDb(csvItem.eanCode, csvFileName, pgmId, "Not a valid eanCode", "WF_EANI-EANCODE-VALIDATION", NEARME_TRANS_TYPE)
        }

    }))
    let csvItemList = [{ productList }, { imageList }, { brandList }, { categoryList }];
    return this.getSourcePathOfCSV(csvItemList, pgmId, productImportConfig);
}

/**
 * Generate CSV for brand,category,.. based on the CSV Item list and upload to S3 for processing import
 * @param {*} csvItemList 
 * @param {*} pgmId 
 * @param {*} productImportConfig 
 * @returns 
 */
module.exports.getSourcePathOfCSV = async (csvItemList, pgmId, productImportConfig) => {
    //Initialize source path
    let sourceS3Path = {
        "productListPath": [],
        "brandListPath": [],
        "categoryListPath": [],
        "imageListPath": []
    };
    //generating csv, upload to S3 bucket and returns the S3 path
    await Promise.all(csvItemList.map(async (item) => {
        //itemType :brandList, categoryList....
        let itemType = Object.keys(item)[0];
        //returns the S3 uploaded path
        sourceS3Path[itemType + "Path"] = await generateCsvAndUploadToS3(item, pgmId, productImportConfig);
    }));
    return sourceS3Path;
}

/**
 *Invoke api to get the details for product from DB EAN master 
 * @param {*} eanCode 
 * @returns 
 */
async function getEANProductDetails(eanCode, tableName) {
    let params = {
        TableName: tableName,
        Key: marshall({
            "hashKey": eanCode
        })
    }
    try {
        //Getting the product details from DB
        let dbItem = await dynamoDB.getItem(params)
        let productDetails = dbItem.Item
        //if the result is empty 
        if (productDetails == undefined || productDetails.data == null) {
            // console.log("For " + eanCode + " No Data Found")
            return null;
        }
        //getting data from DB
        //pass the details to product Import
        return unmarshall(productDetails).data
    } catch (err) {
        console.error(err);
    }
}


/**
 * Generate csv from jsonArray and upload to S3 import bucket
 * @param {*} itemData 
 * @param {*} pgmId 
 * @param {*} productImportConfig 
 */
function generateCsvAndUploadToS3(itemData, pgmId, productImportConfig) {
    let filePathList = [];
    //FileType- productList, imageList 
    let importFileType = Object.keys(itemData)[0]
    let csvFileCount = 1;
    return new Promise(async (resolve, reject) => {
        try {
            //itemLisrt split to generate based on the size 10
            for (i = 0; i < itemData[importFileType].length; i += SPLIT_FILE_SIZE) {
                const newItemList = itemData[importFileType].slice(i, i + SPLIT_FILE_SIZE);
                //file is uploaded in tmp folder
                let fileName = generateFileName(csvFileCount, importFileType, productImportConfig)
                //converting json array data to csv file
                jsonToCsv.json2csvAsync(newItemList).then(items => {
                    fs.writeFileSync(fileName, items)
                })
                csvFileCount++;
                filePathList.push(putImportFileToS3(fileName, pgmId, importFileType))
                //upload the generated csv file to S3
            }
            resolve(await Promise.all(filePathList)
            );
        } catch (err) {
            reject(err);
        }
    })
}

/**
 * Generating file name with date and time and returns the directory
 * @param {*} csvFileCount 
 * @param {*} importFileType 
 * @param {*} productImportConfig 
 * @returns 
 */
function generateFileName(csvFileCount, importFileType, productImportConfig) {
    //Read time zone from configuration 
    if (productImportConfig.timeZone != undefined || productImportConfig.timeZone != "") {
        process.env.TZ = productImportConfig.timeZone;
    } else {
        process.env.TZ = "Asia/Calcutta"
    }
    let date = new Date().toLocaleDateString()
        .replace(/\//g, '-') + "T" + new Date()
            .getHours() + "-" + new Date()
                .getMinutes();
    return "/tmp/" + csvFileCount + importFileType + date + ".csv";
}

/**
 * Importing csvfile to S3
 * @param {*} filePath 
 * @param {*} pgmId 
 * @param {*} fileType 
 */
async function putImportFileToS3(filePath, pgmId, fileType) {
    let csvfileContent = fs.createReadStream(filePath)
    //FilePath: tmp/{fileName}
    let fileName = filePath.split('/').pop()
    let s3FileDir = "";
    switch (fileType) {
        case "imageList": s3FileDir = pgmId + "/_processed/images/" + fileName
            break;
        case "brandList": s3FileDir = pgmId + "/_processed/brand/" + fileName
            break;
        case "productList": s3FileDir = pgmId + "/_processed/product/" + fileName
            break;
        case "categoryList": s3FileDir = pgmId + "/_processed/category/" + fileName
            break;
    }
    let params = {
        Bucket: s3BucketName,
        Key: s3FileDir,
        Body: csvfileContent
    }
    // uploading csv file S3 import bucket
    await s3Object.send(new PutObjectCommand(params))
    return s3BucketName + "/" + s3FileDir
}

/**
 * Generating brand and category for import CSV
 * @param {*} itemDataList 
 * @param {*} pgmId 
 * @param {*} productImportConfig 
 */
module.exports.generateImportCSVs = (itemDataList, pgmId, productImportConfig) => {
    let categoryList = [], brandList = [];
    let uniqueBrandNameList = new Set(), uniqueCategoryNameList = new Set();
    let csvType = Object.keys(itemDataList)[0]
    //For Products:loop through the CSV list and check the starts characte is "?" for brand and category
    if (csvType == "productList") {
        itemDataList[csvType].map(productPayload => {
            //For creating category CSV from productPayload
            if (typeof productPayload.categories == "string" && productPayload.categories.charAt(0) == '?') {
                //Checking the category name exit in the List
                if (!uniqueBrandNameList.has(productPayload.categories)) {
                    //Properties setting for category payload
                    let categoryPayload = {
                        "name": productPayload.categories,
                        "id": "", "description": "", "isHidden": "", "priority": "",
                        "images": "", "hasCategory": "", "hasProduct": "",
                        "parentId": ""
                    }
                    categoryList.push(categoryPayload)
                    //category name adding to the list
                    uniqueCategoryNameList.add(categoryPayload.categories)
                }
            }
            //For creating Brand CSV from productPayload
            if (typeof productPayload.brand == "string" && productPayload.brand.charAt(0) == '?') {
                //Checking the brand name exit in the List
                if (!uniqueBrandNameList.has(productPayload.brand)) {
                    //Properties setting for brand payload
                    let brandPayload = {
                        "name": productPayload.brand,
                        "id": "", "images": "", "priority": ""
                    }
                    brandList.push(brandPayload)
                    //brand name adding to the list
                    uniqueBrandNameList.add(brandPayload.brand)
                }
            }
        })
        let csvItemList = [itemDataList, { brandList }, { categoryList }];
        //Process the File for product ,brand and category List to import to NEARME and return the sourcePath
        return this.getSourcePathOfCSV(csvItemList, pgmId, productImportConfig)
    } else {
        //For other CSV Data type like: brand, category, image process the CSV and return the generated CSV source file path
        return this.getSourcePathOfCSV([itemDataList], pgmId, productImportConfig)
    }
}

