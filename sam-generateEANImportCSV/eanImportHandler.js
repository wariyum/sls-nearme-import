const { S3Client, GetObjectCommand } = require("@aws-sdk/client-s3");
const s3Client = new S3Client({ region: process.env.AWS_REGION });
const csvtojson = require('csvtojson');
const eanImport = require('./eanImportFiles/getEANImportCSV.js')
const eanEntry = require('./eanEntryDB/EANdetailsEntry')
const nearmeTransLog = require("/opt/transLogEntry.js")
const ean_sf = require('./eanEntryDB/sf-ean')
let getProductConfig = require("/opt/getNearmeConfiguration")
const NEARME_TRANS_TYPE = "Import-EAN"
/**
 * Handler for productdetails entry and invoking step function
 * @param {*} event 
 * @param {*} context 
 */
module.exports.eanEntryInDB = async (event, context) => {
    let records = event.Records
    await Promise.all(records.map(async recordItem => {
        let s3UploadedFilePath = recordItem.s3.object.key
        //sample uploadedFilePath : "{pgmId}/eanCodes/product.csv";
        let splitFilePath = s3UploadedFilePath.split("/")
        //checkIng Csv type eanCodes..
        let csvType = splitFilePath[1]
        let csvFileName = splitFilePath[2]
        //If csvType is eanCode entry the data to db
        if (csvType == "eanCodes") {
            const pgmId = splitFilePath[0]
            //get JsOn data of eanCode from imported CSV
            let eanCodeRecords = await getCsvToJsonDataFromS3("nearme-ean-import", s3UploadedFilePath, pgmId, csvFileName);
            //Trans log entry for Ean product Details entry in DB
            nearmeTransLog.logTxnEntry("uploaded", csvFileName, pgmId, "Uploaded eanCode Csv file :" + csvFileName, NEARME_TRANS_TYPE, "started")
            //Invoke eanEntry and skuMapping in DB
            await eanEntry.eanCodeEntryInDB(pgmId, eanCodeRecords, csvFileName)
            //Invoke step function for eanImport
            return ean_sf.invokeEANsf(s3UploadedFilePath)
        }
        else if (csvType == "_processed") {
            return true;
        }
        else {
            //csvType is products, brands, categories or images import
            //s3FilePath: "{pgmId}/{csvType}/{fileName.csv}"
            //Invoking step function for imports (products,brands...)
            return ean_sf.invokeEANsf(s3UploadedFilePath)
        }
    }))
}

/**
 * Using eanCode import product details to Nearme
 * @param {*} event 
 * @param {*} context 
 * @returns 
 */
module.exports.eanDataImport = async (event, context) => {
    let s3UploadedFilePath = event.eanFilePath
    //splitting string s3FilePath to get the pgmId and bucketName
    let splitFilePath = s3UploadedFilePath.split("/")
    //S3 bucket in which the eanCode csv is uploaded
    //s3UploadedFilePath: nearme-ean-import/5-1-NER-1/eanCodes/test.csv
    // const s3BucketName = splitFilePath[0]
    // const pgmId = splitFilePath[1]
    const [s3BucketName, pgmId, csvType, csvFileName] = splitFilePath;
    // const csvFileName = splitFilePath[3]
    //Removing the bucket name from the split values
    splitFilePath.shift()
    //Join s3 uplaoded file path with the split value
    let s3ObjectKey = splitFilePath.join("/")
    let s3Path = {}
    //destination path for importing product to NEARME
    s3Path.destinationS3Path = {
        productListPath: `nearme-import/${pgmId}/import-product`,
        imageListPath: `nearme-import/${pgmId}/import-images/products`,
        brandListPath: `nearme-import/${pgmId}/import-brand`,
        categoryListPath: `nearme-import/${pgmId}/import-category`
    }
    //getting the product import configuration from NEARME CONFIGURATION DDB
    const productImportConfig = await getProductConfig.getNearmeConfig(pgmId, "productImportConfiguration")
    if (csvType == "eanCodes") {
        //Read csv file and convert to JSON Object
        let eanCodeList = await getCsvToJsonDataFromS3(s3BucketName, s3ObjectKey, pgmId, csvFileName);
        //Pass the json Object to the generate the import file , upload to S3 and returns the source and destination path
        s3Path.sourceS3Path = await eanImport.generateImportFilesForEAN(pgmId, eanCodeList, csvFileName, productImportConfig)
        //Trans log entry for generating import CSV
        nearmeTransLog.logTxnEntry("", csvFileName, pgmId, "Generated Import CSV's for eanCode Csv file :" + csvFileName, NEARME_TRANS_TYPE, "processing")
    } else {
        console.log("Importing Data Type: " + csvType)
        let csvData = {}
        //Eg: {productList:{productCsv_JSON_data}}
        csvData[csvType + "List"] = await getCsvToJsonDataFromS3(s3BucketName, s3ObjectKey, pgmId, csvFileName);
        s3Path.sourceS3Path = await eanImport.generateImportCSVs(csvData, pgmId, productImportConfig)
    }
    return s3Path;
}

/**
 * function to get csv file from s3 and convert data to json
 * @param {*} s3BucketName 
 * @param {*} s3ObjectKey 
 * @param {*} s3Config 
 * @returns 
 */
async function getCsvToJsonDataFromS3(s3BucketName, s3ObjectKey, pgmId, fileName) {
    const command = new GetObjectCommand({ Bucket: s3BucketName, Key: s3ObjectKey })
    try {
        const response = await s3Client.send(command)
        let csvDataTxt = await response.Body.transformToString()
        const csvDataToJson = await csvtojson().fromString(csvDataTxt);
        return csvDataToJson;
    } catch (err) {
        //NEARME transactional error entry in DB
        nearmeTransLog.captureErrorToTransErrorDb("", fileName, pgmId, err.message, "WF_EANI-S3-READ-ERROR", NEARME_TRANS_TYPE)
    }
}