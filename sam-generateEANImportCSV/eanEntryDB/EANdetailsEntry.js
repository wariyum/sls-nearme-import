const { DynamoDBClient, PutItemCommand, GetItemCommand } = require("@aws-sdk/client-dynamodb");
const { marshall } = require("@aws-sdk/util-dynamodb");
const fetch = (...args) => import("node-fetch").then(({ default: fetch }) => fetch(...args));
const ssm = require("/opt/config.js");
const dynamoDB = new DynamoDBClient({ region: process.env.AWS_REGION });
const metering = require("/opt/metering.js")
const nearmeTransLog = require("/opt/transLogEntry.js")
const NEARME_TRANS_TYPE = "EAN-IMPORT"

/**
 * PROCESS-1 in import from EAN Details
 * Save EanCode Product details in dynamoDb
 * @param {*} pgmId
 */
module.exports.eanCodeEntryInDB = async (pgmId, eanCodeList, csvFileName) => {
  const tableName = await ssm.getValueFromSSM("EAN_table");
  const goUpcApiToken = await ssm.getValueFromSSM("goUpcApiToken");
  const regExpForEanCode = new RegExp("^[0-9]{13}$");
  for (i = 0; i < eanCodeList.length; i++) {
    //if mapped sku is not empty 
    if (eanCodeList[i].skuId != undefined && eanCodeList[i].skuId != "") {
      //saving the mapped sku and eanCode entry in DB
      mappedSkuEntryInDB(pgmId, eanCodeList[i]);
    }
    //Validating the eanCode
    if (regExpForEanCode.test(eanCodeList[i].eanCode)) {
      //Set Details of product from EAN code
      await setEanProductDetails(
        Number(eanCodeList[i].eanCode),
        pgmId,
        tableName,
        goUpcApiToken,
        csvFileName
      );
    }
  }
  console.log("=======Completed EAN Details entry in DB=========");
}

/**
 * SkuID and eanCode mapping saving in DB
 * @param {*} pgmId
 * @param {*} eancodeDetails
 * @returns
 */
async function mappedSkuEntryInDB(pgmId, eancodeDetails) {
  const params = {
    TableName: "NEARME-EANCODES",
    Item: marshall({
      hashKey: pgmId + "#SkuBarcode",
      pgmId: pgmId,
      eanCode: eancodeDetails.eanCode,
      skuId: eancodeDetails.skuId,
    }),
  };
  try {
    return await dynamoDB.send(new PutItemCommand(params));
  } catch (error) {
    console.error(error);
  }
}

/**
 * Invoke api to get the details for product from DB EAN master
 * If no match found in DB invoke the EAN API and save details in EAN master
 * @param {*} eanCode
 * @param {*} pgmId
 * @param {*} tableName
 * @param {*} goUpcApiToken
 * @returns
 */
async function setEanProductDetails(eanCode, pgmId, tableName, goUpcApiToken, csvFileName) {
  let params = {
    TableName: tableName,
    Key: marshall({
      hashKey: eanCode,
    }),
  };
  try {
    //Getting the product details from DB
    const data = await dynamoDB.send(new GetItemCommand(params));
    if (data.Item != undefined) {
      console.log(`For ${eanCode} the item is present in DB`);
    } else {
      //if not exist create entry in dynamodb and return the product details
      await invokeEanApi(eanCode, tableName, goUpcApiToken, pgmId, csvFileName);
    }
  } catch (error) {
    console.error(error);
  }
}

/**
 * Invoke API to get the productDetails and save in DB
 * @param {*} eanCode
 * @param {*} tableName
 * @param {*} goUpcApiToken
 * @param {*} pgmId
 */
async function invokeEanApi(eanCode, tableName, goUpcApiToken, pgmId, csvFileName) {
  //go-upc api to get the product details
  let url = "https://go-upc.com/api/v1/code/" + eanCode;
  //params for ean APi
  let params = {
    method: "GET",
    headers: {
      authorization: "Bearer " + goUpcApiToken,
    },
  };
  //Invoking the API to get the product details
  return fetch(url, params)
    .then(async (response) => {
      console.log("EANCODE entered in db :" + eanCode)
      let responseBody = await response.json();
      //API invoking counter value update in db
      metering.incMeter(pgmId, "EAN_API_INVOKE")
      if (response.status == 200) {
        //API invoking success counter value update in db
        metering.incMeter(pgmId, "EAN_API_INVOKE_SUCCESS")
        //insert the new product details into the DB
        const data = {
          TableName: tableName,
          Item: marshall({
            hashKey: eanCode,
            data: responseBody,
          }),
        };
        return await dynamoDB.send(new PutItemCommand(data));
      } else {
        //API invoking failure counter value update in db
        metering.incMeter(pgmId, "EAN_API_INVOKE_FAILED")
        //EANCode error details entry in error table
        nearmeTransLog.captureErrorToTransErrorDb(eanCode, csvFileName, pgmId, JSON.stringify(responseBody), "WF_EANI-EAN-API-FAILURE", NEARME_TRANS_TYPE)
        //record the not valid eanCode in DB
        const items = {
          TableName: tableName,
          Item: marshall({
            hashKey: eanCode,
            data: null,
          }),
        };
        return await dynamoDB.send(new PutItemCommand(items));
      }
    })
    .catch((error) => {
      throw error;
    });
}
