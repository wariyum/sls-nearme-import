let { SFN: StepFunctions } = require("@aws-sdk/client-sfn");
const stepFunctions = new StepFunctions({ region: process.env.AWS_REGION });
const ssm = require("/opt/config.js")

/**
 * Invoking eanImport step function
 * @param {*} s3Path 
 */
module.exports.invokeEANsf = async (s3Path) => {
    let s3BucketName = await ssm.getValueFromSSM("EAN_s3_bucket");
    let eanSFarn = await ssm.getValueFromSSM("EAN_SF_arn");
    let inputBody = {
        eanFilePath: s3BucketName + "/" + s3Path
    }
    var params = {
        stateMachineArn: eanSFarn,
        input: JSON.stringify(inputBody)
    }
    console.log("==========Invoked Step function============")
    //start the execution for the ean import step function
    return stepFunctions.startExecution(params)
}
